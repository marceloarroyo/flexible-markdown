//=============================================================================
// HTML renderer
//=============================================================================
const md = require('./md-parser')
const katex = require('katex')

function renderMath(str) {
    let html = 'Katex error'
    try {
        html = katex.renderToString(str);
    } catch (e) {
        if (e instanceof katex.ParseError) {
            html = "katex error in '" + str + "': " + e.message
            .replace(/&/g, "&amp;").replace(/</g, "&lt;")
            .replace(/>/g, "&gt;");
        }
    }
    return html
}

function entag(tag, content, attrs) {
    let result = `<${tag}`
    if (attrs)
        for(let attr in Object.keys(attrs))
            result += ` ${attr}="${attrs[attr]}"`
    result += `>\n${content}\n</${tag}>\n`
}

let renderer = {
    /* inline */
    counters: {fn: 0},
    icode: (text)   => entag('code', text),
    ctag: (tag)     => tag.open + tag.content + tag.close,
    stag: (text)    => txt,
    bold: (text)    => entag('strong', text),
    it:   (text)    => entag('<em>', text),
    img:  (i)       => '<img alt="' + i.alt + '" src="' + i.src + '">',
    link: (l)       => '<a href="#' + l.ref + '">' + l.text + '</a>',
    footnote: (f)   => '<a href="' + f + '">[<sup>' + 
                       (++this.counters.fn) + '</sup>]</a>',
    cite: (c)       => '<a href="#' + c + '">[' + c + ']</a>',
    imath: (m)       => renderMath(m),
    expand: (v)     => v,

    // block renderers
    header: (h)     => entag('h' + h.level, h.text, {id: h.id}),
    code: (c)       => '<pre>' + 
                       entag('code', c.code, {id: c.id, 'class': c.language) +
                       '</pre>\n',
    blockquote(b)   => entag('blockquote', b.text, 
                        b.keyword ? {'class': b.keyword} : null),
    itemsBegin: (i) => '<ul>\n',
    itemsEnd: (i)   => '</ul>\n',
    enumsBegin: ()  => '<ol>\n',
    enumsEnd: ()    => '</ol>\n',
    itemBegin: (i)  => '<li>\n' + i.text + '\n',
    itemEnd: ()     => '</li>\n',
    figure: (f)     => `<figure id="${f.id}">\n` +
                        `<img alt="${f.caption}" src="${f.src}">\n` +
                        entag('figcaption', f.caption) +
                       '</figure>\n',
    footnote: (fn)  => entag('div', fn.text, {id:fn.label,'class':'footnote'},
    cite: (c)       => entag('div', c.text, {id:c.label,'class':'cite'},
    math: (m)       => entag('div', renderMath(m.math), {'class':'equation'}},
}

exports.renderer = renderer

/*
function renderInline(text, ast) {
    let codeCounter = 0
    let code = {}
    let result = text.
    // protect inline code fragments
    replace(/\u0060([^\u0060]+)\u0060/gm, (_,text) => {
        let key = 'code-' + (++codeCounter)
        code[key] = text
        return `<code>${key}</code>`
    }).
    // bold / emphasis
    replace(/\*\*([^\*]+)\*\*/gm, '<b>$1</b>').
    replace(/\*([^\*]+)\*/gm, '<em>$1</em>').
    replace(/_([^_]+)_/gm, '<em>$1</em>').
    // images
    replace(/!\[([^\]]+)\]\(([^\)]+)\)/gm,
            '<img id="$1" alt="$1" src="$2">').
    // links
    replace(/\[([^\]]+)\]\(([^\)]+)\)/gm, '<a href="$2">$1</a>').
    replace(/\[([^\]]*)\]\[([^\]]+)\]/gm, (_, txt, id) => {
        id = id.toLowerCase().replace(' ','-')
        let ref = ast.links[id]
        let url = (ref && ref.url) || ('#' + id)
        let label = ref ? txt : 'reference-error'
        let tag = ref.type == 'footnote' ? 'sup' : 'span'
        return `<a href="${url}"><${tag}>${label}</${tag}></a>`
    }).
    // math
    replace(/\$([^\$]+)\$/gm, (_,m) => renderMath(m)).
    // variable expansion
    replace(/\{{2}(\w+)\}{2}/gm, (_,w) => {
        let isTOC = w == 'toc'
        let nl = isTOC ? '\n' : ''
        let tag = isTOC ? 'nav' : 'span'
        return  `${nl}<${tag} class="${w}">${nl}` +
                (ast.yaml[w.toLowerCase()] || '') + 
                `</${tag}>${nl}${nl}`
    });

    // restore protected inline code fragments
    for (let k in code)
        result = result.replace(`<code>${k}</code>`, `<code>${code[k]}</code>`)
    return result
}

/ toc is an tree of nodes: {label: lbl, href: id, children: [...]}
function renderHTMLToc(toc, filename) {
    let result = ''
    if (toc.children.length > 0) {
        result += '<ol>\n'
        for (let item of toc.children) {
            result += `<li><a href="${filename}/#${item.href}">` + 
                      `${item.label}</a>\n` +
                      renderHTMLToc(item) + '</li>\n'
        }
        result += '</ol>\n'
    }
    return result
}

function renderHTMLHeader(yaml) {
    let html =
        '<!doctype html>\n<html>\n<head>\n  <meta charset="utf8">\n' +
        `  <title>${yaml['title']}</title>\n`
    for (let key of ['author', 'date', 'description', 'keywords'])
        if (yaml[key])
            html += `  <meta name="${key}" content="${yaml[key]}">\n`

    let css = yaml['css']
    if (css)
        for (let f of css)
            html += `  <link rel="stylesheet" href="${f.trim()}">\n`

    html += '</head>\n<body>\n'

    // htmlheader: [title, author, ...]
    if (yaml['htmlheader']) {
        html += '<header>\n'
        for (let key of yaml['htmlheader'])
            html += `<div id="${key}" class="header">${yaml[key]}></div>\n`
        html += '</header>\n'
    }

    return html
}

function renderHTMLFooter(yaml) {
    let html = ''
    let scripts = yaml['scripts'] || []
    
    if (yaml['htmlfooter']) {
        html += '<footer>\n'
        for (let key of yaml['htmlfooter'])
            html += `<div id="${key}" class="footer">${yaml[key]}</div>\n`
        html += '</footer>\n'
    }
    for (let s of scripts)
        html += `<script type="text/javascript" src="${s.trim()}"></script>\n`
    return html + '</body>\n</html>\n'
}

function renderHTMLCaption(block, ast) {
    if (block.caption && block.caption.length > 1) {
        let tag = 'p'
        if (block.type == 'figure') 
            tag = 'figcaption'
        else if (block.type == 'table') 
            tag = 'caption'
        return (tag == 'p' ? '<p class="caption">' : `<${tag}>`) +
               renderInline(block.caption, ast) + `</${tag}>\n`
    }
    return ''
}

function renderHTMLBody(ast) {
    let yaml = ast.blocks[0].meta
    let html = ''
    let itemsCtx = [{type: 'begin', indent: 0}]
    let hasYaml = Object.keys(ast.blocks[0].meta).length > 0

    for (let block of ast.blocks) {
        // close items contexts
        if (ctx.type == block.type && ctx.indent == block.indent)
            html += '</li>\n'
        else
            while (block.indent < ctx.indent) {
                let tag = ctx.type == 'item' ? 'ul' : 'ol'
                html += `</li>\n</${tag}>\n`
                if (itemsCtx.length > 1)
                    itemsCtx.pop()
                ctx = itemsCtx[itemsCtx.length - 1]
            }

        switch (block.type) {
            case 'code': {
                let id = block.id ? ` id="${block.id}"` : ''
                let cls = block.cls ? ` class="${block.cls}"` : ''
                html += `<div${id}>\n<pre><code${cls}>\n` +
                        block.text +
                        '\n</code></pre>\n' +
                        renderHTMLCaption(block, ast) + '</div>\n'
                break
            }
            case 'header': {
                html += `<h${block.level} id="${block.id}">` +
                        renderInline(block.text, ast) +
                        `</h${block.level}>\n`
                break
            }
            case 'blockquote': {
                let bq_ast = md.parse(block.text)
                let id = block.id ? ` id="${block.id}"` : ''
                let cls = block.cls ? ` class="${block.cls}"` : ''
                bq_ast.links = ast.links
                html += `<blockquote${id}${cls}>\n` +
                        renderHTML(bq_ast) + renderHTMLCaption(block, ast) +
                        '</blockquote>\n'
                break
            }
            case 'item':
            case 'enum': {
                let tag = block.type == 'item' ? 'ul' : 'ol'
                if (ctx.type != block.type || block.indent > ctx.indent + 2)
                {
                    itemsCtx.push({type: block.type, indent: block.indent})
                    html += `<${tag}>\n`
                }
                html += '<li>' +
                        renderInline(block.text, ast) +
                        '</li>\n'
                break
            }
            case 'table': {
                let id = block.id ? ` id="${block.id}"` : ''
                let cls = block.cls ? ` class="${block.cls}"` : ''
                html += `<table${id}${cls}>\n` +
                        renderHTMLCaption(block, ast) + '<tr>\n'
                for (let col=0; col < block.data[0].length; col++)
                    html += `<th class="tcol-${col}">` +
                            renderInline(block.data[0][col], ast) +
                            '</th>\n'
                html += '</tr>\n'
                for (let r=1; r < block.data.length; r++) {
                    html += '<tr>\n'
                    for (let col=0; col < block.data[r].length; col++)
                        html += `<td class="tcol-${col}">` +
                                renderInline(block.data[r][col], ast) +
                                '</td>\n'
                    html += '</tr>\n'
                }
                html += '</table>\n'
                break
            }
            case 'figure': {
                let ext = block.src.match(/\.(.*)$/)
                let tag = 'figure'
                let cls = block.cls ? ` class="${block.cls}"` : ''
                if (ext[1] == 'mp4' || ext[1] == 'ogv')
                    tag = 'video'
                else if (ext[1] == 'mp3' || ext[1] == 'ogg')
                    tag = 'audio'
                html += `<${tag} id="${block.alt}"${cls}>\n`
                if (tag == 'figure')
                    html += `<img src="${block.src}" alt="${block.alt}">\n`
                else
                    html += `<source src="${block.src}" type="${tag}/${ext}">\n`
                html += renderHTMLCaption(block) + `</${tag}>\n`
                break
            }
            case 'math': {
                let id = block.id ? ` id="${block.id}"` : ''
                let cls = block.cls ? ` class="mathblock ${block.cls}"` : ''
                html += `<div${id}${cls}>\n` +
                        renderMath(block.text) +
                        renderHTMLCaption(block) + '\n</div>\n'
                break
            }
            case 'cite':
            case 'footnote': {
                html += `<p id="${block.key}" class="${block.type}">\n` +
                        `<span>${block.label}</span>: ` +
                        renderInline(block.text, ast) + '\n</p>\n'
                break
            }
            case 'separator': {
                let tag = 'hr'
                if (block.cls.length > 0)
                    tag = block.cls[0] == '/' ? '/div' : 'div'
                let cls = tag[0] != '/' ? ` class="${block.kind}"` : ''
                if (tag != 'hr' && tag[0] != '/')
                    cls = ` class="${block.kind} ${block.cls}"`
                html += `<${tag}${cls}>\n`
                if (block.cls.length > 1 && block.cls[0] == '/') {
                    // open a new <div>
                    cls = block.cls.substring(1)
                    html += `<div class="${block.kind} ${cls}">\n`
                }
                break
            }
            case 'html': {
                html += block.text
                break
            }
            case 'paragraph': {
                html += '<p>\n' +
                        renderInline(block.text, ast) +
                        '</p>\n'
                break
            }
        }
    }
    return html
}

function renderHTML(ast, filename) {
    let yaml = ast.blocks[0].meta
    let result = renderHTMLHeader(yaml)
    let tocLevel = yaml['toclevels']
    if (tocLevel) {
        let levels = {min: parseInt(tocLevel[0]), max: parseInt(tocLevel[1])}
        yaml['toc'] = renderHTMLToc(md.genTOC(ast, levels), filename)
    }
    result += renderHTMLBody(ast)
    result += renderHTMLFooter(yaml)
    return result
}

exports.render = renderHTML
exports.header = renderHTMLHeader
exports.body   = renderHTMLBody
exports.footer = renderHTMLFooter
exports.toc    = renderHTMLToc

*/
