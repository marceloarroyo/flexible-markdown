//=============================================================================
// Markdown parser
// Author: Marcelo Arroyo
// Description:
// 1. The input text is divided in paragraphs (text blocks separated by 2 or more
//    newlines.
// 2. Each block is parsed by parseBlocks(blocks) function.
//    This function generate an abstract syntax tree (ast): An object
//    {
//      yaml: {...}, // yaml meta-data
//      blocks: [],  // array of blocks
//      links: {}    // object of links
//    }
// Each block have the following common fields:
// - type: 'header', 'block', ...
// - indent: indentation level
// Almost every block (except ) have a "text" field
// Container blocks (blockquotes and items) have a "blocks" array field for its
// inner blocks
//=============================================================================

// return a tree of 
function genTOC(ast, levels) {
    let yaml = ast.blocks[0].meta,
        result = {label: yaml.title, href: 'root', children:[]}

    for (let block of ast.blocks) {
        if (block.type == 'header' && 
            block.level >= levels.min && 
            block.level <= levels.max) {
            let target = result.children
            for (let l = levels.min; l < block.level; l++)
                target = target[target.length - 1].children
            target.push({label: block.text, href: block.id, children: []})
        }
    }
    return result
}

function inItems(c, b) {
    return  (c.type == 'items' && b.type == 'item' && /[-+*]/.test(b.item[0]))
            || 
            (c.type == 'enums' && b.type == 'enum' && /[\d]/.test(b.item[0]))
}

function isContainer(b) {
    return ['blockquote', 'items', 'enums', 'item', 'enum'].includes(b.type)
}

function canHaveCaption(block) {
    return ['code', 'table'].includes(b.type)
}

let _inline_counter = 0

// Inline parser: replace inline patterns (emphasis, italics, code, ...) with
// tags for later (during rendering) replacement
function inline(text, ast) {
    let rp = (tag, obj) =>  {
        let key = '%%inline-' + tag + '-' + (++ _inline_counter) + '%%'
        ast.yaml[key] = obj
        return key
    }
    let result = text.
    
    // code
    replace(/\u0060([^\u0060]+)\u0060/gm, (_,txt) => rp('icode', txt)).
    
    // html (closed) tag
    replace(/(<\w+>)([^<]+)(<\/\w+>)/gm, 
        (_, o, t, c) => rp('ctag', {open: t, content: t, close: c})).
    
    // html single tag (example: <br>
    replace(/<\w+\/?>/, (t) => rp('stag', t)).
    
    // bold / emphasis
    replace(/\*\*([^\*]+)\*\*/gm, (_, txt) => rp('bold',txt)).
    replace(/\*([^\*]+)\*/gm, (_, txt) => rp('it',txt)).
    replace(/_([^_]+)_/gm, (_, txt) => rp('it',txt)).
    
    // images
    replace(/!\[([^\]]+)\]\(([^\)]+)\)/gm, 
        (_,c,s) => rp('image', {alt:c, src:s})).
    
    // links
    replace(/\[([^\]]+)\]\(([^\)]+)\)/gm, (_,t,r) => rp('link',{text:t,ref:r})).
    
    // reference
    replace(/\[([^\]]*)\]\ *\[([^\]]+)\]/gm, 
        (_, txt, id) => rp('ref',{key: id, text: txt})).

    // footnotes
    replace(/\[\^([^\]]*)\]/gm, (_, key) => rp('footnote', key)).
    
    // cites
    replace(/\[\#([^\]]*)\]/gm, (_, key) => rp('cite', key)).
    
    // math
    replace(/\$([^\$]+)\$/gm, (_,m) => rp('imath', m)).
    
    // variable expansion
    replace(/\{{2}(\w+)\}{2}/gm, (_,w) => rp('expand', ast.yaml[w] || w))
    
    return result
}

function appendTo(ast, block) {
    let target = ast
    let last = ast.blocks[ast.blocks.length - 1] || {type: '', indent: 0}
    while (isContainer(last) && block.indent > last.indent) {
        target = last
        last = target.blocks[target.blocks.length-1] || {type: '', indent: 0}
    }
    if (inItems(last, block) && block.indent == last.indent) {
        target = last
        last = target.blocks[target.blocks.length-1]
    }
    if (block.type == 'caption' && canHaveCaption(last))
        last.caption = block
    else {
        let elem = block
        if (block.type == 'item' && 
            (!inItems(target, block) || block.indent > target.indent))
            // it is the first item: create items container
            elem = {
                type:   block.type + 's',
                indent: block.indent,
                blocks: [block] 
            }
        target.blocks.push(elem)
    }
}

function parseBlocks(blocks) {
    let ast = {yaml: {}, blocks: [], links: {}}
    
    for (let block of blocks) {
        // yaml
        if (block.startsWith('---') && block.endsWith('---')) {
            let lines = block.split('\n')
            for (let line of lines) {
                let pair = line.trim().match(/^([\w\d_]+)\:(.+)/)
                if (pair) {
                    let key = pair[1].toLowerCase()
                    pair[2] = pair[2].trim()
                    if (pair[2].startsWith('[') && pair[2].endsWith(']'))
                        ast.yaml[key] =
                            pair[2].substring(1, pair[2].length-1).split(/, ?/)
                    else
                        ast.yaml[key] = pair[2]
                }
            }
            continue
        }

        // code
        let match = block.match(/^( {4,})[\u0060\~]{3,}(.*)/)
        if (match) {
            let lines = block.split('\n')
            if (lines.length > 2) {
                let contentLines = lines.slice(1, lines.length-1)
                let leadingSpacesRE = new RegExp(`^ ${match[1].length}`)
                let content = []
                for (let l of contentLines)
                    content.push(l.replace(leadingSpacesRE, ''))
                appendTo(ast, {
                    type: 'code',
                    indent: match[1].length,
                    language: match[2].trim(),
                    code: content.join('\n')
                })
                continue
            }
        }

        // header
        match = block.match(/^( *)(#{1,6}) (.*)/)
        let match2 = block.match(/^( *)(.*)\n([-=]{3,})$/)
        if (match || match2) {
            let hlevel = match ? match[2].length
                               : (match2[3][0] == '=' ? 1 : 2)
            let txt = match ? match[3] : match2[2]
            let id = block.match(/ \{#([\w-]+)\} *$/)
            if (id)
                txt = txt.replace(id[0], '')
            appendTo(ast, {
                type: "header",
                indent: match ? match[1].length : match2[1].length,
                level: hlevel,
                text: inline(txt, ast),
                id: id ? id[1] : txt.replace(/[_*]/gm,'').replace(/ /gm,'-')
            })
            continue
        }

        // blockquote
        match = block.match(/^( *)> /)
        if (match) {
            let content = block.replace(/^ *> /gm, '')
            let keyword = content.match(/^\w+/)
            appendTo(ast, {
                type: 'blockquote',
                indent: match[1].length,
                keyword: keyword,
                blocks: mdParse(content).blocks
            })
            continue
        }

        // item
        match = block.match(/^( *)([-+*]|(:?\d+\.)) /)
        if (match) { 
            appendTo(ast, {
                type: /\d/.test(match[1][0]) ? 'enum' : 'item',
                item: match[2].replace('.', ''),
                indent: match[1].length,
                text: inline(block.substring(match[0].length).trim(), ast),
                blocks: []
            })
            continue
        }

        // Image ![id](url "title")
        match = block.match(/^( *)!\[([^\]]+)\]\(([^ ]+)\) \"([^\"]+)\" *$/)
        if (match) {
            appendTo(ast, {
                type: 'figure',
                indent: match[1].length,
                id: match[2],
                caption: inline(match[4], ast),
                src: match[3]
            })
            continue
        }

        // references, footnotes, cites and captions
        match = block.match(/^( *)\[([^\]]+)\]\:(.*)$/m)
        if (match) {
            let t = {'^': 'footnote', '#': 'cite'}
            let keyParts = match[2].toLowerCase().split(' ')
            let ref = {
                type: t[ match[2][0] ] || 'reference',
                indent: match[1].length,
                label: match[2]
            }
            if (ref.type == 'reference' && keyParts.length > 1)
                ref.type = 'caption'
            if (ref.type != 'reference') {
                ref.counter = ++counters[ref.type]
                ref.text = inline(match[3].trim(), ast)
                appendTo(ast, ref)
            }
            else {
                let start = match[3].indexOf('"') + 1
                let end = match[3].indexOf('"', start)
                let txt = match[3].substring(start, end)
                ref.text = txt.length > 0 ? inline(txt, ast) : null
                ref.href = match[3].substring(0, start-1).trim()
            }
            if (ref.type == 'footnote' || ref.type == 'cite')
                key = match[2].substring(1)
            ast.links[key] = ref
            continue
        }

        // table
        match = block.match(/ \| /)
        if (match) {
            let rows = block.split('\n')
            let rowsData = []
            let header = []
            for (let row = 0; row < rows.length; row++) {
                    let columns = rows[row].split(/ *\| */)
                    let cols = []
                    for (let col=0; col<columns.length; col++) {
                        let c = columns[col].trim()
                        switch (row) {
                            case 0: 
                                header.push({text: columns[col], align: 'l'})
                                break
                            case 1: {
                                if (c[0] == ':' && c[c.length-1] == ':')
                                    header[col].align = 'c'
                                else if (c[0] == '=' && c[c.length-1] == ':')
                                    header[col].align = 'r'
                                break
                            }
                            default:
                                cols.push(inline(col)) 
                        }
                    }
                    if (cols.length > 0)
                        rowsData.push(cols)
            }
            appendTo(ast, {
                type: 'table',
                indent: block.match(/^ */)[0].length,
                header: header,
                rows: rowsData
            })
            continue
        }

        // separator: --- optional-tag
        // optional-tag: opening | / | /closing-opening
        match = block.match(/^( *)([\-=_]{5,})( ?\/?\w*)/)
        if (match) {
            appendTo(ast, {
                type: 'separator',
                indent: match[1].length,
                kind: match[2][0],
                cls: match[3].trim()
            })
            continue
        }

        // math
        match = block.match(/^( *)\$\$([^\$]+)\$\$ *$/)
        if (match) {
            appendTo(ast, {
                type: 'math',
                indent: match[1].length,
                math: match[2]
            })
            continue
        }

        // html tag
        match = block.match(/^( *)<[^>]+> *$/)
        if (match) {
            appendTo(ast, {
                type: 'html',
                indent: match[1].length,
                html: match[0].trim()
            })
            continue
        }

        // paragraph
        appendTo(ast, {
            type: 'paragraph',
            indent: block.match(/^ */)[0].length,
            text: inline(block, ast)
        })
    } // end for

    return ast
}

function mdParse(text) {
    let blocks = text.
        // split contiguous items in paragraphs
        replace(/\n *- /g, (m) => `\n${m}`).
        replace(/\n *\d+\. /g, (m) => `\n${m}`).
        // same with references
        replace(/\n\[[^\]]+\]\:/g, (m) => `\n${m}`).
        // collapse more than 3 adyacent newlines to 2
        replace(/\n{3,}/g, '\n\n').
        // replace tabs by four spaces
        replace(/\t/g, '    ').
        // split input paragraps in blocks
        split('\n\n')
    let ast = parseBlocks(blocks)
    return ast
}

// A rendeerer is an object with methods named as block types.
// For each container type block t, must have two methods: tBegin() and tEnd() 

function renderInline(text, ast) {
    return
    text.replace(
        /\%\%inline\-(\w+)\-\d+\%\%/gm,
        (match, type) => {
            let obj = ast.yaml[match]
            if (type == 'ref') {
                let link = ast.links[obj.key]
                if (!obj.text || obj.text.length == 0)
                    obj.text = link.text || link.href
                obj.ref = link.href
                type = 'link'
            }
            renderer[type](obj)
        }
    )
}

function mdRender(ast, renderer) {
    let output = ''
    for (let block of ast.blocks) {
        if (isContainer(block)) {
            output += renderer[block.type + 'Begin'](block)
            if (block.text)
                output += renderInline(block, ast)
            let nast = {yaml: ast.yaml, blocks: block.blocks, links: ast.links}
            output += mdRender(nast, renderer)
            output += renderer[block.type + 'End'](block)
        } else if (block.text)
            output += renderer[block.type](block)
    }
}

/*
exports.parse =  mdParse
exports.genTOC = genTOC
exports.render = mdRender
*/
